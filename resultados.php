<?php
  include_once("db.php");

  $sql = "SELECT * FROM formularios";
  $result = mysqli_query($conn, $sql);
?>

<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inicial-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <title>Resultado</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <h2><center>Resultados Formularios</center></h2>
    <hr>
    <table border="1">
      <thead>
        <tr role="row">
          <th>ID</th>
          <th>Nombre </th>
          <th>Apellidos </th>
          <th>Edad</th>
          <th>mayor o menod de edad</th>
          <th>Tipo de boletos</th>
          <th>Contraseña</th>
          <th>Email</th>
          <th>sexo</th>
          <th>entradas con</th>
          <th>Paises</th>
          <th>Color de la entrada</th>
          <th>Fecha de nacimiento</th>
          <th>Hora de llegada</th>
          <th>Archivo prueba</th>
          <th>Área de Personal solicitada</th>
          <th>Info extra </th>
        </tr>
      </thead>
      <tbody>
        <?php while( $data = mysqli_fetch_assoc($result) ) { ?>
          <tr style="text-align: center;">
            <td><?php echo $data ['id']; ?></td>
            <td><?php echo $data ['nombre']; ?></td>
            <td><?php echo $data ['apellidos']; ?></td>
            <td><?php echo $data ['edad']; ?></td>
            <td><?php echo $data ['mayoria_edad']; ?></td>
            <td><?php echo $data ['tipo_boleto']; ?></td>
            <td><?php echo $data ['contrasena']; ?></td>
            <td><?php echo $data ['mail']; ?></td>
            <td><?php echo $data ['sexo']; ?></td>
            <td><?php echo $data ['entradas']; ?></td>
            <td><?php echo $data ['pais']; ?></td>
            <td><?php echo $data ['color']; ?></td>
            <td><?php echo $data ['nacimiento']; ?></td>
            <td><?php echo $data ['hora_llegada']; ?></td>
            <td><?php echo $data ['archivo_vacunacion']; ?></td>
            <td><?php echo $data ['area_personal']; ?></td>
            <td><?php echo $data ['mas_info']; ?></td>				  
          </tr>
        <?php } $conn -> close(); ?>
      </tbody>
    </table>
    <br>
    <center><button type="button" onClick="history.go(-1);">Volver</button></center>
  </body>
</html>