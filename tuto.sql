-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para tuto
CREATE DATABASE IF NOT EXISTS `tuto` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tuto`;

-- Volcando estructura para tabla tuto.formularios
CREATE TABLE IF NOT EXISTS `formularios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  `apellidos` varchar(50) NOT NULL DEFAULT '0',
  `edad` int(11) DEFAULT '0',
  `mayoria_edad` varchar(50) DEFAULT '0',
  `tipo_boleto` varchar(50) DEFAULT '0',
  `contrasena` varchar(50) NOT NULL DEFAULT '0',
  `mail` varchar(50) NOT NULL DEFAULT '0',
  `sexo` varchar(50) NOT NULL DEFAULT '0',
  `entradas` varchar(50) DEFAULT '0',
  `pais` varchar(50) NOT NULL DEFAULT '0',
  `color` varchar(50) DEFAULT '0',
  `nacimiento` date NOT NULL,
  `hora_llegada` time DEFAULT '00:00:00',
  `archivo_vacunacion` varchar(50) DEFAULT 'VACIO',
  `area_personal` varchar(50) DEFAULT '0',
  `mas_info` varchar(50) DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

